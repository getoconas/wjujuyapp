var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
        //Agregamos la función en el evento deviceready para iniciar Angular
        document.addEventListener("deviceready", initAngular, false); 
    },

    // deviceready Event Handler
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
    },

};


        //</script>
   

    

    // onSuccess Callback
    // This method accepts a Position object, which contains the
    // current GPS coordinates
    //
    /* Obtiene posición del usuario para recuperar datos estación meteorológica
    automática (EMA) más cercana */
    var onSuccess = function(position) {
        var geoju = new XMLHttpRequest();
        var proju = new XMLHttpRequest();

        geoju.open('GET', 'http://api.wunderground.com/api/7ede035d4b1a7e9d/conditions/q/' + position.coords.latitude + ',' + position.coords.longitude + '.json', true);
        proju.open('GET', 'http://api.wunderground.com/api/6b7c21981bf5895a/hourly/q/' + position.coords.latitude + ',' + position.coords.longitude + '.json', true);

        geoju.send();
        proju.send();
        //Se recuepera datos (T°, H%, etc) segun la propiedad que tienen en el .json
        geoju.onload = function() {
            var geojuInfo = JSON.parse(geoju.responseText);
            console.log(geojuInfo);
            //Localidad usuario WU
            document.getElementById('Lgeoju').innerHTML = geojuInfo.current_observation.display_location.city;
            //Tempertura actual en grados celsius
            document.getElementById('Tgeoju').innerHTML = geojuInfo.current_observation.temp_c;
            //Humedad relativa actual
            document.getElementById('Hgeoju').innerHTML = geojuInfo.current_observation.relative_humidity;
            //Icono de estado actual (despejado, nublado, lluvia, etc)
            document.getElementById('Igeoju').src = geojuInfo.current_observation.icon_url;
            //Estado del tiempo (Despejado, Nublado, Lluvia, etc)
            var condicion = geojuInfo.current_observation.weather;
            
            if (condicion == "Clear") {
                document.getElementById('Cgeoju').innerHTML = "Despejado";
            } else if (condicion == "Partly Cloudy") {
                document.getElementById('Cgeoju').innerHTML = "Algo Nublado";
            } else if (condicion == "Mostly Cloudy") {
                document.getElementById('Cgeoju').innerHTML = "Parcialmente Nublado";
            } else if (condicion == "Cloudy" || condicion == "Overcast") {
                document.getElementById('Cgeoju').innerHTML = "Nublado";
            } else if (condicion == "Rain") {
                document.getElementById('Cgeoju').innerHTML = "Nublado con Lluvia";             
            } else if (condicion == "Thunderstorm") {
                document.getElementById('Cgeoju').innerHTML = "Nublado con Tormenta";
            } else if (condicion == "Sleet") {
                document.getElementById('Cgeoju').innerHTML = "Nublado con Aguanieve";
            } else if (condicion == "Snow") {
                document.getElementById('Cgeoju').innerHTML = "Nublado con Nevada";
            }
        }

        proju.onload = function() {
            /*Pronóstico próxima hora según la ubicación del usuario*/
            var projuInfo = JSON.parse(proju.responseText);
            
            document.getElementById('hour1').innerHTML = projuInfo.hourly_forecast[0].FCTTIME.hour + ":00";
            document.getElementById('temp1').innerHTML = projuInfo.hourly_forecast[0].temp.metric;
            document.getElementById('hum1').innerHTML = projuInfo.hourly_forecast[0].humidity;
            document.getElementById('icon1').src = projuInfo.hourly_forecast[0].icon_url;
            
            var conHour0 = projuInfo.hourly_forecast[0].condition;
            
            if (conHour0 == "Clear") {
                document.getElementById('est1').innerHTML = "Despejado";
            } else if (conHour0 == "Partly Cloudy") {
                document.getElementById('est1').innerHTML = "Algo Nublado";
            } else if (conHour0 == "Mostly Cloudy") {
                document.getElementById('est1').innerHTML = "Parcialmente Nublado";
            } else if (conHour0 == "Cloudy" || conHour == "Overcast") {
                document.getElementById('est1').innerHTML = "Nublado";
            } else if (conHour0 == "Chance of Rain") {
                document.getElementById('est1').innerHTML = "Probabilidad de Lluvias";
            } else if (conHour0 == "Chance of a Thunderstorm") {
                document.getElementById('est1').innerHTML = "Probabilidad de Tormentas";
            }

            document.getElementById('hour3').innerHTML = projuInfo.hourly_forecast[3].FCTTIME.hour + ":00";
            document.getElementById('temp3').innerHTML = projuInfo.hourly_forecast[3].temp.metric;
            document.getElementById('hum3').innerHTML = projuInfo.hourly_forecast[3].humidity;
            document.getElementById('icon3').src = projuInfo.hourly_forecast[3].icon_url;
            
            var conHour3 = projuInfo.hourly_forecast[3].condition;
            
            if (conHour3 == "Clear") {
                document.getElementById('est3').innerHTML = "Despejado";
            } else if (conHour3 == "Partly Cloudy") {
                document.getElementById('est3').innerHTML = "Algo Nublado";
            } else if (conHour3 == "Mostly Cloudy") {
                document.getElementById('est3').innerHTML = "Parcialmente Nublado";
            } else if (conHour3 == "Cloudy" || conHour == "Overcast") {
                document.getElementById('est3').innerHTML = "Nublado";
            } else if (conHour3 == "Chance of Rain") {
                document.getElementById('est3').innerHTML = "Probabilidad de Lluvias";
            } else if (conHour3 == "Chance of a Thunderstorm") {
                document.getElementById('est3').innerHTML = "Probabilidad de Tormentas";
            }
        }

        $.ajax({
            url: 'http://api.openweathermap.org/data/2.5/forecast?lat=' + position.coords.latitude + '&lon=' + position.coords.longitude + '&units=metric&lang=es&appid=ab99eda62dba0763eaf9bbdf48be7b7e',
            type: 'GET',
            dataType: 'jsonp',
            success: function(data) {
                $('#Clocalidad').html(data.city.name);
            }
        });
    };

    // onError Callback receives a PositionError object
    //
    function onError(error) {
        alert('code: '    + error.code    + '\n' +
              'message: ' + error.message + '\n');
    }

    navigator.geolocation.getCurrentPosition(onSuccess, onError);

app.initialize();
