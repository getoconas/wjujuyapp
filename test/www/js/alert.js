//Función para obtener las alertas del Servicio Meterológico Nacional (SMN).
//Realizado por el equipo de Tiempo en Jujuy.

var smnObject = new XMLHttpRequest();

smnObject.open('GET', 'https://ws.smn.gob.ar/alerts/type/AL', true);

smnObject.send();

smnObject.onload = function() {
	var smnInfo = JSON.parse(smnObject.responseText);
	console.log(smnInfo);
	var text;
	var cobertura;
	var flag;
	//Si array tiene algo entra al if
	console.log(smnInfo.length);
	if (smnInfo.length > 0) {
		text = "<div><h4><strong>ALERTAS METEOROL&Oacute;GICAS EN EL PA&Iacute;S</strong></h4>";
		for (var i = 0; i < smnInfo.length; i++) {
			cobertura = "";
			for (var key in smnInfo[i].zones){
	    		cobertura += " - " + smnInfo[i].zones[key];
			}
			text += "<div><h4>" + smnInfo[i].status + ": " + smnInfo[i].title + " - Horas: " + smnInfo[i].hour + "</h4></div></p>" + smnInfo[i].description + "</p>" + "<p><strong> Zona de Cobertura: " + cobertura + "</strong></p>" + "</p>El informe se actualizar&aacute; a horas: " + smnInfo[i].update + "</p>";
			cobertura = "";
			flag = true;
		}
		text += "</div>";
		document.getElementById("smnAlerta").innerHTML = text;
	} else {
		text = "<div><h4><strong>NO HAY ALERTAS METEOROL&Oacute;GICAS VIGENTES</strong></h4></div>";
		flag = false;
		document.getElementById("smnAlerta").innerHTML = text;
	}
	
	//Si flag es verdadero entra para cambiar el color a rojo, si es falso a verde.
	if (flag) {
		var elemento = document.getElementById("smnAlertaColor");
		elemento.className += " alert alert-danger";
	} else {
		var elemento = document.getElementById("smnAlertaColor");
		elemento.className += " alert alert-success";
	}

	document.getElementById("smnBadge").innerHTML = smnInfo.length;
}
