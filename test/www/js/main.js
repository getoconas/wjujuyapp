var weatherObject = new XMLHttpRequest();
var laAlmona = new XMLHttpRequest();
var palpala = new XMLHttpRequest();
var elCarmen = new XMLHttpRequest();
var sanPedro = new XMLHttpRequest();
var palmaSola = new XMLHttpRequest();
var perico = new XMLHttpRequest();
var monterrico = new XMLHttpRequest();

//Obtener datos de la api de wunderground según el ID de la EMA
weatherObject.open('GET', 'http://api.wunderground.com/api/d8b7ac53c0f71e22/conditions/q/pws:IJUJUYPR4.json', true);
laAlmona.open('GET', 'http://api.wunderground.com/api/5c279e7506b312bc/conditions/q/pws:IJUJUYSA4.json', true);
palpala.open('GET', 'http://api.wunderground.com/api/c53b1d4229bccdc8/conditions/q/pws:IJUJUYCA2.json', true);
elCarmen.open('GET', 'http://api.wunderground.com/api/40e4fad324352694/conditions/q/pws:IJUJUYEL3.json', true);
sanPedro.open('GET', 'http://api.wunderground.com/api/f577e42a17aabb02/conditions/q/pws:IJUJUYPR2.json', true);
palmaSola.open('GET', 'http://api.wunderground.com/api/24350adefbeb2b9d/conditions/q/pws:IJUJUYPA2.json', true);
perico.open('GET', 'http://api.wunderground.com/api/a0b40f1f6ba579f3/conditions/q/pws:IJUJUYPE4.json', true);
monterrico.open('GET', 'http://api.wunderground.com/api/da9972c7aa341e74/conditions/q/pws:IJUJUYMO3.json', true);

weatherObject.send();
laAlmona.send();
palpala.send();
elCarmen.send();
sanPedro.send();
palmaSola.send();
perico.send();
monterrico.send();

//Función para obtener datos de la api, ya sea temperaturas, humedad, coordenadas, etc.
weatherObject.onload = function() {
	var weatherInfo = JSON.parse(weatherObject.responseText);
	document.getElementById('currentTemp').innerHTML = weatherInfo.current_observation.temp_c;
	document.getElementById('humedad').innerHTML = weatherInfo.current_observation.relative_humidity;
	document.getElementById('icono').src = weatherInfo.current_observation.icon_url;
}

laAlmona.onload = function() {
	var laAlmonaInfo = JSON.parse(laAlmona.responseText);
	document.getElementById('currentTemp1').innerHTML = laAlmonaInfo.current_observation.temp_c;
	document.getElementById('humedad1').innerHTML = laAlmonaInfo.current_observation.relative_humidity;
	document.getElementById('icono1').src = laAlmonaInfo.current_observation.icon_url;
}

palpala.onload = function() {
	var palpalaInfo = JSON.parse(palpala.responseText);
	document.getElementById('currentTemp2').innerHTML = palpalaInfo.current_observation.temp_c;
	document.getElementById('humedad2').innerHTML = palpalaInfo.current_observation.relative_humidity;
	document.getElementById('icono2').src = palpalaInfo.current_observation.icon_url;
}

elCarmen.onload = function() {
	var elCarmenInfo = JSON.parse(elCarmen.responseText);
	document.getElementById('currentTemp3').innerHTML = elCarmenInfo.current_observation.temp_c;
	document.getElementById('humedad3').innerHTML = elCarmenInfo.current_observation.relative_humidity;
	document.getElementById('icono3').src = elCarmenInfo.current_observation.icon_url;
}

sanPedro.onload = function() {
	var sanPedroInfo = JSON.parse(sanPedro.responseText);
	console.log(sanPedroInfo);
	document.getElementById('currentTemp4').innerHTML = sanPedroInfo.current_observation.temp_c;
	document.getElementById('humedad4').innerHTML = sanPedroInfo.current_observation.relative_humidity;
	document.getElementById('icono4').src = sanPedroInfo.current_observation.icon_url;
}

palmaSola.onload = function() {
	var palmaSolaInfo = JSON.parse(palmaSola.responseText);
	document.getElementById('currentTemp5').innerHTML = palmaSolaInfo.current_observation.temp_c;
	document.getElementById('humedad5').innerHTML = palmaSolaInfo.current_observation.relative_humidity;
	document.getElementById('icono5').src = palmaSolaInfo.current_observation.icon_url;
}

perico.onload = function() {
	var pericoInfo = JSON.parse(perico.responseText);
	document.getElementById('currentTemp6').innerHTML = pericoInfo.current_observation.temp_c;
	document.getElementById('humedad6').innerHTML = pericoInfo.current_observation.relative_humidity;
	document.getElementById('icono6').src = pericoInfo.current_observation.icon_url;
}

monterrico.onload = function() {
	var monterricoInfo = JSON.parse(monterrico.responseText);
	document.getElementById('currentTemp7').innerHTML = monterricoInfo.current_observation.temp_c;
	document.getElementById('humedad7').innerHTML = monterricoInfo.current_observation.relative_humidity;
	document.getElementById('icono7').src = monterricoInfo.current_observation.icon_url;
}
