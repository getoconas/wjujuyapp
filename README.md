# Tiempo en Jujuy [Deprecated]

[Página web - Tiempo en Jujuy](http://www.tiempoenjujuy.com)

## Descripción

Esta aplicación tiene por objetivo informar el estado del tiempo de toda la provincia de Jujuy.
A través de la ubicación del usuario puede obtener automáticamente el estado actual del tiempo.

## Instalación
Clonar el repositorio
```
git clone https://gitlab.com/getoconas/wjujuyapp.git
```

Instala depdencias, plugins y platforms
```
npm run tejscript
```

Plataformas disponibles
- Android
- BlackBerry10 (apk)

```
cordova run browser
```

